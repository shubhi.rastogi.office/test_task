import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import "../styling/Header.css";
import Polygon from "../images/Polygon.png";
import tools from "../images/tools.png";
import bitcoin from "../images/bitcoin 1.png";
const useStyles = makeStyles((theme) => ({
  root: {
    marginLeft: "30%",
    width: "10%",
    overflow: "inherit",
  },
  rootOne: {
    marginLeft: "1%",
    width: "10%",
  },
}));

export default function SecondHeader() {
  const classes = useStyles();
  return (
    <div
      style={{
        padding: "0.7%",
      }}
    >
      <div style={{ flexDirection: "row", display: "flex" }}>
        <div
          style={{
            alignItems: "center",
            justifyContent: "center",
            display: "flex",
            flexDirection: "row",
            // marginLeft: "1%",
            minWidth: "12%",
            maxWidth: "15%",
            // borderStyle: "solid",
            backgroundColor: "#F3F3F3",
          }}
        >
          <img src={tools} style={{ width: "30px" }} />
          <div style={{ color: "grey", marginLeft: "15%" }}>Tools</div>
        </div>

        <div
          style={{
            marginLeft: "10%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <img src={bitcoin} style={{ width: "50px" }} />
          <div className="font-color-real">Real - time bitcoin graph</div>
        </div>

        <div className={classes.root}>
          <div class="dropdown">
            <button class="dropbtnPink">
              Commodity 1
              <img src={Polygon} />
            </button>
            <div class="dropdown-content">
              <a>Link 1</a>
              <a>Link 2</a>
              <a>Link 3</a>
            </div>
          </div>
        </div>
        <div className={classes.rootOne}>
          <div class="dropdown" style={{ backgroundColor: "#5843BE" }}>
            <button class="dropbtnPurple">
              Commodity 2
              <img src={Polygon} />
            </button>
            <div class="dropdown-content1">
              <a>Link 1</a>
              <a>Link 2</a>
              <a>Link 3</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
