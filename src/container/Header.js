import React from "react";
import "../styling/Header.css";
import Vector from "../images/Vector.png";
import Polygon from "../images/Polygon.png";
import search from "../images/search.png";
import deactive from "../images/deactive.png";
import user from "../images/user.png";
import InputBase from "@material-ui/core/InputBase";
import { fade, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  search: {
    //   position: 'relative',
    //   borderStyle:"solid",
    borderRadius: theme.shape.borderRadius,
    //   backgroundColor: fade(theme.palette.common.white, 0.15),
    //   '&:hover': {
    //     backgroundColor: fade(theme.palette.common.white, 0.25),
    //   },
    marginLeft: 0,
    width: "30%",
    //   textAlign:"center",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  },
  // searchIcon: {
  //   padding: theme.spacing(0, 2),
  //   height: '100%',
  //   position: 'absolute',
  //   pointerEvents: 'none',
  //   display: 'flex',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // },
  // inputRoot: {
  //   color: 'inherit',
  // },
  // inputInput: {
  //   padding: theme.spacing(1, 1, 1, 0),
  //   // vertical padding + font size from searchIcon
  //   paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
  //   transition: theme.transitions.create('width'),
  //   width: '100%',
  //   [theme.breakpoints.up('sm')]: {
  //     width: '12ch',
  //     '&:focus': {
  //       width: '20ch',
  //     },
  //   },
  // },
}));

export default function Header() {
  const classes = useStyles();
  return (
    <div style={{ padding: "1%",  }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        //   borderStyle: "solid",
        //   borderWidth: 1,
        }}
      >
        <img src={Vector} style={{ width: "40px" }} />
        <div style={{ fontWeight: "bold" }} className="font-color">
          ACADZA
        </div>
        <div
          style={{
            marginLeft: "15%",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
          className="font-color-jr"
        >
          Activity
          <img src={Polygon} style={{ width: "12%", marginLeft: "2%" }} />
        </div>
        <div
          style={{
            marginLeft: "1%",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            textAlign: "center",
          }}
          className="font-color-jr"
        >
          Tool Guide
          <img src={Polygon} style={{ width: "10%", marginLeft: "2%" }} />
        </div>
        <div
          className={classes.search}
          style={{
            marginLeft: "30%",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <img src={search} style={{ width: "10px", marginLeft: "2%" }} />
          <InputBase
            style={{ marginLeft: "5%", color: "purple" }}
            placeholder="Search…"
            inputProps={{ "aria-label": "search" }}
          />
        </div>
        <div
          style={{ marginLeft: "10%", alignItems: "center", display: "flex" }}
        >
          <img src={deactive} style={{ width: "25px" }} />
        </div>
        <div
          style={{ marginLeft: "2%", alignItems: "center", display: "flex" }}
        >
          <img src={user} style={{ width: "35px" }} />
          <img src={Polygon} style={{ width: "15%", marginLeft: "2%" }} />
        </div>
      </div>
    </div>
  );
}
