import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
// import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { Line } from "@reactchartjs/react-chart.js";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import accuracyup from "../images/accuracyup.png";
import Dashboard from "../images/dashboard.png";
import Rank from "../images/rankup.png";
import Speedup from "../images/speedup.png";
import Revision from "../images/revision.png";
import testCreator from "../images/testcreator.png";
import Assignment from "../images/assignment 1.png";
import study from "../images/study.png";
import Backlog from "../images/backlog.png";
import formula from "../images/formula.png";
import Chart from "./Chart";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  // necessary for content to be below app bar
  // toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    marginTop: theme.spacing(18),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(5),
  },
  listItemText: {
    fontSize: "15px",
    fontFamily: "Gill Sans",
    paddingBlock: "4%",
  },
}));

function PersistentDrawerLeft(props) {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <List>
        {[
          "Dashboard",
          "Backlog Remover",
          "Rank up",
          "Speed up",
          "Accuracy up",
          "Revision",
          "Test Creator",
          "Assignment Creator",
          "Study Material",
          "Formula Sheet",
        ].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              <img
                src={
                  index == 0
                    ? Dashboard
                    : index == 1
                    ? Backlog
                    : index == 2
                    ? Rank
                    : index == 3
                    ? Speedup
                    : index == 4
                    ? accuracyup
                    : index == 5
                    ? Revision
                    : index == 6
                    ? testCreator
                    : index == 7
                    ? Assignment
                    : index == 8
                    ? study
                    : formula
                }
                style={{ width: "30%" }}
              />
            </ListItemIcon>
            <ListItemText
              primary={text}
              classes={{ primary: classes.listItemText }}
            />
          </ListItem>
        ))}
      </List>
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;
 
  return (
    <div className={classes.root}>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Chart />
        {/* <Line data={data} options={options} /> */}
      </main>
    </div>
  );
}

PersistentDrawerLeft.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default PersistentDrawerLeft;
