import React from 'react'
import { Line } from '@reactchartjs/react-chart.js'
import '../styling/Chart.css'
const data = {
  labels: ['1', '2', '3', '4', '5', '6'],
  datasets: [
    {
      label: 'Data set 1',
      data: [34,234,3242,3234,],
      fill: false,
      lineTension: 0.5,
      backgroundColor: '#C30F70',
      borderColor: '#C30F70',
      // borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: '#C30F70',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: '#C30F70',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 5,
      pointHitRadius: 10,
    },
    {
      label: 'Data set 2',
      data: [23,45,45,665],
      fill: false,
      lineTension: 0.5,
      backgroundColor: '#5843BE',
      borderColor: '#5843BE',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: '#5843BE',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: '#5843BE',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 5,
      pointHitRadius: 10,
    },
  ],

}

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
  aspectRatio: 10,  // not being respected, still at `2`
    responsive: true,
    // maintainAspectRatio: false
}

const Chart = () => (
  <div style={{maxWidth:"90%",maxHeight:"50%", marginBlock:"1%", flex:1,display:"flex"}}>
   
    <Line data={data} options={options} 
   />
  </div>
)

export default Chart